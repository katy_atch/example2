package edu.baylor.ecs.si;


public class TimerException extends Exception {

	// TODO Q1 what is this and why do we need it?
	// It is a version number used during deserialization to verify that
	// the sender and receiver of a serialized object have loaded classes
	// for that object that are compatible. 
	private static final long serialVersionUID = -2414489560888085344L;

	/**
     * Constructs a new exception with {@code null} as its detail message.
     * The cause is not initialized, and may subsequently be initialized by a
     * call to {@link #initCause}.
     */
    public TimerException() {
        super();
    }

    /**
     * Constructs a new exception with the specified detail message.  The
     * cause is not initialized, and may subsequently be initialized by
     * a call to {@link #initCause}.
     *
     * @param   message   the detail message. The detail message is saved for
     *          later retrieval by the {@link #getMessage()} method.
     */
    // TODO Q2 why do we need to override this?
    // We override some of the constructors for exceptions so that we can
    // provide an error message along with the exception if we wish to give
    // more description about what caused the exception. We can even provide
    // a cause along with the error message string if we want.
    public TimerException(String message) {
        super(message);
    }

    /**
     * Constructs a new exception with the specified detail message and
     * cause.  <p>Note that the detail message associated with
     * {@code cause} is <i>not</i> automatically incorporated in
     * this exception's detail message.
     *
     * @param  message the detail message (which is saved for later retrieval
     *         by the {@link #getMessage()} method).
     * @param  cause the cause (which is saved for later retrieval by the
     *         {@link #getCause()} method).  (A <tt>null</tt> value is
     *         permitted, and indicates that the cause is nonexistent or
     *         unknown.)
     */
    public TimerException(String message, Throwable cause) {
        super(message, cause);
    }
    
    // TODO Q3 why we did not override other Exception methods?
    // For our particular test, we only needed to add the option
    // to provide a message and cause for the TimerException. 

}
